import React, {useContext, useState, useEffect} from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import {NoteContext} from '../../../context/notes';
import { Row, Container, Title } from '../../../AzizaAkhmedova';
import { Note } from '../../../AynurAsadova';

export const Actual = () => {

    const  {notes} = useContext(NoteContext);

    const [items, setItems] = useState([]);

    useEffect(() => {setItems(notes.filter(item => item.status === false))}, [notes])

    return(
        <div>
            <Container>
            <Title>Notes to complete</Title>
                <Row>
                    {

                        items && 
                        items.map(note => (
                            <StyledNav to = {`/notes/${note.id}`} key = {note.id} >
                                <Note key = {note.id} note= {note}/>
                            </StyledNav> 
                        ))
                    }
                </Row>
            </Container>
        </div>
    )
}

const StyledNav = styled(NavLink)`
    text-decoration: none;
`;
