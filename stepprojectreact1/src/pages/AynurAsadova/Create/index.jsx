import React, {useContext} from 'react';

import { Container, Title } from '../../../AzizaAkhmedova';
import {NoteContext} from '../../../context/notes'
import { NoteForm } from '../../../AynurAsadova/components';


export const Create = ({history}) => {
    const {addNote} = useContext(NoteContext)
    const createNoteSuccess = note => {
        addNote(note);
        history.push("/");
    }
    return(
        <Container>
            <Title>Create</Title>
            <NoteForm 
            submitBtnText = "Create" 
            onSuccessSubmit = {createNoteSuccess}/>
        </Container>
    )
};
