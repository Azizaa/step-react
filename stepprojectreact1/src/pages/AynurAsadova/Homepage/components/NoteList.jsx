import React, {useContext} from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import { NoteContext } from '../../../../context/notes';
import { Note } from '../../../../AynurAsadova';
import { Row } from '../../../../AzizaAkhmedova';
 
export const NoteList = () => {
    
    const {notes} = useContext(NoteContext);

    return(
        <Row>
            {
                notes.map(note => (
                    <StyledNav to = {`/notes/${note.id}`} key = {note.id} >
                        <Note key = {note.id} note= {note}/>
                    </StyledNav>
                    ))
            }
        </Row>
    )
}

const StyledNav = styled(NavLink)`
    text-decoration: none;
`
