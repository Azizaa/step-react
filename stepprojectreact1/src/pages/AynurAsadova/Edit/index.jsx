import React, {useState, useContext} from 'react';
import styled from 'styled-components';
import { NoteContext } from '../../../context/notes';



export const Edit = ({initial = {}, history, match: {params: {id}}}) => {

    const {notes} = useContext(NoteContext);
    const note = notes.find(item => item.id ==id )

    const [fields, setFields] = useState({
        ...note,
        ...initial
    });
    
    const onChange = e => {
        const {name, value} = e.target;
        setFields(field => ({
            ...field,
            [name]: value
        }))
    }

    const putMethod = {
        method: 'PUT', 
        headers: {
         'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(fields) 
       }
       

    const onSubmit = async (e) => {
        e.preventDefault();
        let validation = true;
        //
        for(let key in fields){
            if(fields [key] === "") {
                validation = false;
                alert(`please, fill ${key} field`)
            }
        }
        if(validation) {
            fetch(`http://localhost:3001/notes/${note.id}`, putMethod);
            history.push("/");
        }
    }

       

    return(
        <Form onSubmit = {onSubmit}>
            <Input 
                type = "text" 
                name = "title"
                value = {fields.title}
                onChange = {onChange}
                />
            <Textarea 
                type = "text" 
                name = "text" 
                value = {fields.text}
                onChange = {onChange}/>
            
            <ColorContainer>
            <h4
            style = {{color: "white"}}
            >Color: </h4>
                <RadioLabel color = "rgba(188,28,83,.8)">
                    <input 
                        type = "radio" 
                        name = "color" 
                        value = "rgba(188,28,83,.8)"
                        checked = {fields.color === "rgba(188,28,83,.8)"}
                        onChange = {onChange} />
                    <span></span>
                </RadioLabel>

                <RadioLabel color = "rgba(105,188,183,.8)">
                    <input 
                        type = "radio" 
                        name = "color" 
                        value = "rgba(105,188,183,.8)"
                        checked = {fields.color === "rgba(105,188,183,.8)"}
                        onChange = {onChange} />
                    <span></span>
                </RadioLabel>

                <RadioLabel color = "rgba(62,13,179,.8)">
                    <input 
                        type = "radio" 
                        name = "color" 
                        value = "rgba(62,13,179,.8)"
                        checked = {fields.color === "rgba(62,13,179,.8)"}
                        onChange = {onChange} />
                    <span></span>
                </RadioLabel>

                <RadioLabel color = "rgba(83,23,104,.8)">
                    <input 
                        type = "radio" 
                        name = "color" 
                        value = "rgba(83,23,104,.8)"
                        checked = {fields.color === "rgba(83,23,104,.8)"}
                        onChange = {onChange} />
                    <span></span>
                </RadioLabel>
            </ColorContainer>

            <Submit>Save</Submit>
        </Form>
        
    )
}

const Form = styled.form`
    max-width: 500px;
    margin: 30px auto;
    padding: 30px 20px;
    background-color: rgba(124, 76, 116, .9);
    border-radius: 15px;

`;

const inputStyles = `
    display: block;
    border: 2px solid transparent;
    transition: all .3s ease;
    background-color: white;
    width: 100%;
    padding: 10px 10px;
    border-radius: 5px;

    &:focus {
        border-color: rgb(131,0,106);
        outline: none;
    }

`

const ColorContainer = styled.div`
    display: flex;
    align-items: center;
    margin: 10px 0;
    h4 {
        margin: 0 25px 0 0;
    }
`;

const Input = styled.input`
    ${inputStyles}
`;
const Textarea = styled.textarea`
    ${inputStyles};
    height: 100px;
    margin-top: 10px;
    resize: none;
`;
const RadioLabel = styled.label`
    input {
        display: none;
    }

    span {
        display: inline-block;
        width: 30px;
        height: 30px;
        border-radius: 100%;
        margin: 0 10px;
        background-color: ${p => p.color};
        border: 3px solid transparent;
        transition: all .3s ease;
        cursor: pointer;
    }

    input: checked + span {
        border-color: rgb(124, 76, 116);
    }
`;
const Submit = styled.button`
    ${inputStyles};
    text-transform: uppercase;
    font-weight: bold;
    background-color: rgb(112, 26, 91);
    color: white;
`;

