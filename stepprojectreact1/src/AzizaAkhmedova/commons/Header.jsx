import React from 'react';
import styled from 'styled-components';
import {NavLink} from 'react-router-dom';
import {Icon} from './Icon';

export const Header = () => {
    return(
        <Container>

            <StyledLogo exact to = "/">
                <Logo><LogoName>Happy Notes</LogoName><AppLogo src = "/photos/robot.png" alt = "App Logo" /></Logo>
            </StyledLogo>

            <Nav>
                <StyledNavLink to = "/actualNotes">
                    Actual</StyledNavLink>
                <StyledNavLink to = "/archivedNotes">
                    <LogoContainer>
                        <div>
                            <Icon src = '/photos/archive.svg' alt = "archive icon"/>
                        </div>
                        <div>
                            Archive
                        </div>
                    </LogoContainer>
                        
                </StyledNavLink>

                <StyledNavLink to = "/create">
                <LogoContainer>
                        <div>
                            <Icon src = '/photos/create.svg' alt = "create icon"/>
                        </div>
                        <div>
                            Create
                        </div>
                    </LogoContainer>
                </StyledNavLink>
            </Nav>

        </Container>
    )
}

const Container = styled.header`
    background: rgba(0,0,0,.3);
    padding: 5px 50px;
    display: flex;
    justify-content: space-between;
    align-items: center;

`;

const Nav = styled.div`
    display: flex;
`

const StyledLogo = styled(NavLink)`
    text-decoration: none;
`;

const LogoName = styled.div`
    margin-right: -60px;
    margin-left: 55px;
    color: rgba(255,255,255,.8);
`

const Logo = styled.div`
    text-decoration: none;
    font-size: 25px;
    font-weight: bold;
    display: flex; 
    align-items: center;
`;

const AppLogo = styled.img`
    width: 70px;
    height: 70px;
    margin-left: 80px;
`;

const StyledNavLink = styled(NavLink)`
    display: inline-block;
    color: rgba(255,255,255,.6);
    font-weight: bold;
    text-decoration: none;
    margin: 0 15px;
    padding: 10px 15px;
    min-width: 150px;
    text-align: center;
    background-color: rgba(112, 26, 91, .8);
    border-radius: 25px;
    border: 2px solid transparent;
    transition: all .3s ease-in-out;

    span {
        margin-right: 10px;

    }

    &.active {
        border-color: rgb(131,0,106);
    }

`
const LogoContainer = styled.div`
    margin: 2px 0 0 6px;
    text-align: center;
    display: flex;
    align-items: center;
    height: 20px;
`

