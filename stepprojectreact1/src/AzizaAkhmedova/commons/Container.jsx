import styled from 'styled-components';

export const Container = styled.div`
    max-width: 1200px;
    padding: 0px 90px;
    overflow-y: auto;
    margin: 0 auto;
`;