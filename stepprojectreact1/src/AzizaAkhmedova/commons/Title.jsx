import styled from 'styled-components';

export const Title= styled.div`
    line-height: 60px;
    font-size: 35px;
    background-color: rgba(0,0,0,.5);
    border-radius: 15px;
    margin: 20px auto 40px;
    width: 50%;
    text-align: center;
    color: rgb(255,255,255);
`
    // background-color: rgba(238, 56, 148, .8);